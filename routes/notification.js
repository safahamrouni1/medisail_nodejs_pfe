const { query } = require('express');
const express = require('express');
const connection= require ('../connection');
const router = express.Router();
var auth = require('../services/authentification');
var checkRole = require('../services/checkRole');


router.get("/fetchNotification",auth.authenticateToken,checkRole.checkRole,(req,res)=>{
    connection.query("SELECT * FROM notifications",(err,result)=>{
        if(!err)
        {
           res.send(result)
        }
        else
        console.log(err);
    });
});

router.get("/getNotificationByUser/:id_client",auth.authenticateToken,checkRole.checkRole,(req,res)=>{
    const id_client= req.params.id_client;
    connection.query('select * from notifications where id_client=? order by date',id_client,(err, result) => {
     if(err)
     {
      console.log(err)
     }
     else
       res.send(result);
    })
 });


 router.post('/addNotification',auth.authenticateToken,checkRole.checkRole,(req,res)=>{
    let notifications = req.body;

                sql = "insert into notifications(notifications,type,imei,id_client,date,read_stat,etat_delete) values (?,?,?,?,CURRENT_TIMESTAMP,1,1)";
                connection.query(sql,[notifications.notifications,notifications.type,notifications.imei,notifications.id_client],(err, results) => {
                    console.log("err3");
                      if(!err){
                          console.log("err4");
  
                          return res.status(200).json({message:"Registered"});
                      }else{
                          console.log("err5");
  
                           return res.status(500).json(err);
                      }
                  })
              
          
         
  })

  router.patch('/update',auth.authenticateToken,checkRole.checkRole,(req, res) => {
    let notifications = req.body;
    const query = 'update notifications set notifications=?,type=?,imei=?,id_client=?,read_stat=?,etat_delete=? WHERE id=?';
    connection.query(query,[notifications.notifications,notifications.type,notifications.imei,notifications.id_client,notifications.read_stat,notifications.etat_delete,notifications.id],(err,results)=>{
        if(!err)
        {
            if (results.affectedRows = 0) {
                res.status(404).json({ message: 'notifications id does not exist' });
              } else {
                res.status(200).json({ message: 'notifications  updated successfully' });
              }
        }
        else
        return res.status(500).json(err);
    })
})


module.exports = router;