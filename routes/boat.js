const { query } = require('express');
const express = require('express');
const connection= require ('../connection');
const router = express.Router();
var auth = require('../services/authentification');
var checkRole = require('../services/checkRole');


router.get("/fetch",(req,res)=>{
  const options = req.query.id_client;
  console.log(options)
      sql ="select * from boats where id_client=?";
      connection.query(sql,[options],(err,result)=>{
        if (err) {
          console.log(err);
          res.send({
            message: "Error fetching boats",
          });
        } else if (result.length > 0) {
          res.send({
            message: "Boats found",
            data: result,
          });
        } else {
          res.send({
            message: "Boats not found",
          });
        }
        });
    })

/*     router.get("/getBoatsByboat/:id_client",(req,res)=>{
    const id_client= req.params.id_client;
    connection.query('select * from boats where id_client=?',id_client,(err, result) => {
     if(err)
     {
      console.log(err)
     }
     else
       res.send(result);
    })
    
 })
 */

 
 router.get("/getSingleBoat/:id",(req,res)=>{

    const id= req.params.id;
    connection.query('select * from boats where id=?',id,(err, result) => {
     if(err){console.log(err) }
     else
     res.send({
      message :'get single boat',
      data:result
    });
    })
    
 })
 
 
 router.post('/addBoat',auth.authenticateToken,(req,res)=>{
    let boat = req.body.dataa;
    const id_client = req.body.id_client;
    console.log(boat,id_client);
       sql ="select * from boats where name=?";
      connection.query(sql,[boat.name],(err,results)=>{
          if(!err)
          {
              if(results.length<=0){
                sql = "insert into boats(imei_box,imei_phone,name,type,brand,model,weigth,heigth,registration_nb,insurance_nb,id_client,date_ajout) values (?,?,?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP)";
                connection.query(sql,[boat.imei_box,boat.imei_phone,boat.name,boat.type,boat.brand,boat.model,boat.weigth,boat.heigth,boat.registration_nb,boat.insurance_nb,id_client],(err, results) => {
            //if(err){console.log(err);}
            res.send({
                message:'DATA INSERTED '
            })
                  })
              }else{
                res.send({
                    message:'BOAT NAME ALREADY EXIST '
                    
                })
                 // return res.status(400).json({message:"Boat name already exist"});
                  
              }
          }
          else
          return res.status(500).json(err);
      })
  })

  router.patch('/update/:id', (req, res) => {
    let gID = req.params.id;
    let boat = req.body;
    const query = 'update boats set imei_box=?,imei_phone=?,name=?,type=?,brand=?,model=?,weigth=?,heigth=?,registration_nb=?,insurance_nb=? WHERE id=?';
    connection.query(query,[boat.imei_box,boat.imei_phone,boat.name,boat.type,boat.brand,boat.model,boat.weigth,boat.heigth,boat.registration_nb,boat.insurance_nb,gID],(err,results)=>{
        if(!err)
        {
            if (results.affectedRows = 0) {
                res.status(404).json({ message: 'Boat id does not exist' });
              } else {
                res.status(200).json({ message: 'Boat profile updated successfully' });
              }
        }
        else
        return res.status(500).json(err);
    })
})

/* router.delete('/deleteBoat',(req, res) => {
    let qID = req.params.id;
    let query = `DELETE  FROM boats WHERE id='${qID}' `;
    connection.query(query,(err,results)=>{
        if(err){console.log(err);}
          res.send({
            message:'data deleted'
          })
           
    })
})
 */
router.delete('/deleteBoat/:id',(req, res) => {
  let id = req.params.id;
  const query = 'DELETE  FROM boats  WHERE id=?';
  connection.query(query,[id],(err,results)=>{
      if(!err)
      {
          if (results.affectedRows = 0) {
              res.status(404).json({ message: 'boat id does not exist' });
            } else {
              res.status(200).json({ message: 'BOAT DELATED' });
            }
      }
      else
      return res.status(500).json(err);
  })
}) 
module.exports = router;