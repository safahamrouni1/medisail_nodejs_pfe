const { query } = require('express');
const express = require('express');
const connection= require ('../connection');
const router = express.Router();
var auth = require('../services/authentification');
var checkRole = require('../services/checkRole');


router.get("/fetch",(req,res)=>{
    connection.query("SELECT * FROM io_history",(err,result)=>{
        if(!err)
        {
           res.send(result)
        }
        else
        console.log(err);
    });
});

router.get("/getHistorySpeedByimei",(req,res)=>{
   var query= "select h.id,h.imei,h.speed from io_history as h INNER JOIN boats as b where h.imei = b.imei_box";
    connection.query(query,(err, result) => {
     if(!err)
     {
      return res.status(200).json(result);
     }
     else
     return res.status(500).json(err);
    })
 });

 router.get("/getHistoryById/:imei",(req,res)=>{
    const imei= req.params.imei;
    connection.query('select * from io_history where imei=? order by date',imei,(err, result) => {
     if(err)
     {
      console.log(err)
     }
     else
       res.send(result);
    })
 });



 router.get("/getHistorybatteryByimei",(req,res)=>{
    var query= "select h.id,h.imei,h.battery_level from io_history as h INNER JOIN boats as b where h.imei = b.imei_box";
     connection.query(query,(err, result) => {
      if(!err)
      {
       return res.status(200).json(result);
      }
      else
      return res.status(500).json(err);
     })
  });
 
  router.get("/getHistoryCarburantByimei",(req,res)=>{
    var query= "select h.id,h.imei,h.analog_input from io_history as h INNER JOIN boats as b where h.imei = b.imei_box";
     connection.query(query,(err, result) => {
      if(!err)
      {
       return res.status(200).json(result);
      }
      else
      return res.status(500).json(err);
     })
  });
 

 
  
module.exports = router;