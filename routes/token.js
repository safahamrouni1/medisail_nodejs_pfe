const { query } = require('express');
const express = require('express');
const connection= require ('../connection');
const router = express.Router();


router.get("/fetchToken",(req,res)=>{
    connection.query("SELECT * FROM fcm_tokens",(err,result)=>{
        if(!err)
        {
           res.send(result)
        }
        else
        console.log(err);
    });
});

router.get("/getTokenByUser/:id_client",(req,res)=>{
    const id_client= req.params.id_client;
    connection.query('select * from fcm_tokens where id_client=?',id_client,(err, result) => {
     if(err)
     {
      console.log(err)
     }
     else
       res.send(result);
    })
 });

module.exports = router;