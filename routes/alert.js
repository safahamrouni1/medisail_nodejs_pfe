const { query } = require('express');
const express = require('express');
const connection= require ('../connection');
const router = express.Router();
var auth = require('../services/authentification');
var checkRole = require('../services/checkRole');


router.get("/fetch",(req,res)=>{
    connection.query("SELECT * FROM alerts",(err,result)=>{
        if(!err)
        {
           res.send(result)
        }
        else
        console.log(err);
    });
});

router.get("/getalertsUser",(req,res)=>{
   var query= "select a.id,a.telephone,a.imei,a.latitude,a.longitude,a.sos,a.pompe,a.anti_theft,a.anti_mouillage,a.alert_time,a.level_alert,a.read_stat,a.id_assistant,a.status_resolve,c.id_client as IdClient ,c.firstname as firstNameClient,c.lastname as lastNameClient,c.telephone as telephoneClient from alerts as a INNER JOIN client as c where a.id_client = c.id_client";
    connection.query(query,(err, result) => {
     if(!err)
     {
      return res.status(200).json(result);
     }
     else
     return res.status(500).json(err);
    })
 });

 router.get("/getAlertByUser/:id_client",(req,res)=>{
    const id_client= req.params.id_client;
    connection.query('select * from Alerts where id_client=? order by alert_time',id_client,(err, result) => {
     if(err)
     {
      console.log(err)
     }
     else
       res.send(result);
    })
 });

 router.post('/addAlert',(req,res)=>{
    let alerts = req.body;

                sql = "insert into alerts(id_client,imei,latitude,longitude,sos,pompe,anti_theft,anti_mouillage,alert_time,level_alert,read_stat,id_assistant,status_resolve) values (?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP,?,?,?,?)";
                connection.query(sql,[alerts.id_client,alerts.imei,alerts.latitude,alerts.longitude,alerts.sos,alerts.pompe,alerts.anti_theft,alerts.anti_mouillage,alerts.level_alert,alerts.read_stat,alerts.id_assistant,alerts.status_resolve],(err, results) => {
                    console.log("err3");
                      if(!err){
                          console.log("err4");
  
                          return res.status(200).json({message:"Registered"});
                      }else{
                          console.log("err5");
  
                           return res.status(500).json(err);
                      }
                  })
              
          
         
  })

  router.patch('/update',(req, res) => {
    let alerts = req.body;
    const query = 'update alerts set id_client=?,imei=?,latitude=?,longitude=?,sos=?,pompe=?,anti_theft=?,anti_mouillage=?,level_alert=?,read_stat=?,id_assistant=?,status_resolve=? WHERE id=?';
    connection.query(query,[alerts.id_client,alerts.imei,alerts.latitude,alerts.longitude,alerts.sos,alerts.pompe,alerts.anti_theft,alerts.anti_mouillage,alerts.level_alert,alerts.read_stat,alerts.id_assistant,alerts.status_resolve,alerts.id],(err,results)=>{
        if(!err)
        {
            if (results.affectedRows = 0) {
                res.status(404).json({ message: 'alerts id does not exist' });
              } else {
                res.status(200).json({ message: 'alerts  updated successfully' });
              }
        }
        else
        return res.status(500).json(err);
    })
})


module.exports = router;