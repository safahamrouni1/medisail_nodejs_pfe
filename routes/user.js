const express = require('express');
const connection= require ('../connection');
const router = express.Router();

const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const { query } = require('express');
require('dotenv').config();
var auth = require('../services/authentification');
var checkRole = require('../services/checkRole');

router.post("/register",(req,res)=>{
  /*   const firstname = req.body.firstname;
    const lastname = req.body.lastname;
    const email = req.body.email;
    const adresse_postale = req.body.adresse_postale;
    const code_postale = req.body.code_postale ;
    const telephone = req.body.telephone;
    const password= req.body.password; */
    user=req.body;
     sql ="select * from client where email=?";
    connection.query(sql,[user.email],(err,results)=>{
        console.log("err1");
        if(!err)
        {
            if(results.length<=0){
                sql ="insert into client(firstname,lastname,adresse_postale,code_postale,telephone,password,email,etat_activer,etat_delete,etat_validation,role)values(?,?,?,?,?,?,?,0,0,0,'client')"
                console.log("err2");
                connection.query(sql,[user.firstname,user.lastname,user.adresse_postale,user.code_postale,user.telephone,user.password,user.email],(err,results)=>{
                    console.log("err3");
                    if(!err){
                        console.log("err4");

                        return res.status(200).json({message:"Registered"});
                    }else{
                        console.log("err5");

                         return res.status(500).json(err);
                    }
                })
            }else{
                return res.status(400).json({message:"email already exist"});
            }
        }
        else
        return res.status(500).json(err);
    });
});

router.post('/login', (req, res) => {

    let user = req.body;
    sql= "SELECT * FROM client WHERE email = ?"
    connection.query(sql,[user.email], (err, results) => {
      if (!err) {
            if (results.length <= 0 || results[0].password != user.password) {
                return res.status(401).json({message:"incorrect username or password"});
            }else if(results[0].etat_activer === 0){
                return res.status(401).json({message:"wait for admin approval"});
            }
            else if(results[0].password == user.password){
            const response ={ email: results[0].email,id_client: results[0].id_client}
            console.log(response);
            const accessToken =jwt.sign(response,process.env.ACCESS_TOKEN,{expiresIn:'8h'})
            res.status(200).json({token:accessToken,response,id_client:results[0].id_client,lastname:results[0].lastname,firstname:results[0].firstname});
            } 
            else{
                return res.status(400).json({message:"something went wrong.please try agin"});
            }
      } else {
        return res.status(500).json(err);
      }
    });
  });
var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth:{
        user: process.env.EMAIL,
        pass: process.env.PASSWORD,
    }
})

router.post('/forgotpassword', (req, res) => {
    const user = req.body;
    sql = "select email,password from client where email=?";
    connection.query(sql,[user.email], (err, results) => {
        if (!err) {
              if (results.length <= 0 ) {
                  return res.status(200).json({message:"We can't find this email"});
              }
              else{
                var mailOptions = {
                    from: process.env.EMAIL,
                    to: results[0].email,
                    subject:'Your Password by Medisail connect',
                    html: '<p><b>Your login details for Medsail connect </b><br><b>Email:</b>'+results[0].email+'<br><b>Password:</b>'+results[0].password+'</p>'
                };
                transporter.sendMail(mailOptions,(error,info)=>{
                    if(error){
                        console.log(error);

                    }else{
                        console.log('email sent:'+info.response);
                    }
                });
                return res.status(200).json({message:"password send seccufly for your mail"});

              }
        } else {
          return res.status(500).json(err);
        }
      })
})

router.get('/get', auth.authenticateToken,checkRole.checkRole,(req, res) => {
    connection.query("SELECT * FROM client where role='user'",(err,results)=>{
        if(!err)
        {
        return res.status(200).json(results);
        }
        else
        return res.status(500).json(err);
    });
})

router.patch('/update',auth.authenticateToken,checkRole.checkRole, (req, res) => {
    let user = req.body;
    const query = 'update client set lastname=?, firstname=?, email=? WHERE id_client=?';
    connection.query(query,[user.lastname,user.firstname,user.email,user.id_client],(err,results)=>{
        if(!err)
        {
            if (results.affectedRows = 0) {
                res.status(404).json({ message: 'User id does not exist' });
              } else {
                res.status(200).json({ message: 'User profile updated successfully' });
              }
        }
        else
        return res.status(500).json(err);
    })
})
 
router.get('/checkToken',auth.authenticateToken,(req,res) => {
        return res.status(200).json({message:"true"});    
    })

router.post('/changePassword',auth.authenticateToken,(req,res) => {
    const user = req.body;
    const email = res.locals.email;
    var query =" select * from client where email=? and password=?";
    connection.query(query,[email,user.oldPassword],(err,results)=>{
        if(!err)
        {
            if (results.length <= 0) {
                res.status(404).json({ message: 'inccorect old password ' });
              } else if(results[0].password== user.oldPassword) {
                query = "update client set password=? where email=? ";
                connection.query(query,[user.newPassword,email],(err,results)=>{
                    if(!err){
                        return res.status(200).json({message:"Password uptaded successfuly"})
                    }
                    else {
                        return res.status(500).json(err);
                    }
                })
              }
              else {
                res.status(400).json({ message: 'Somthing went wrong.Please try agin' });
              }
        }
        else
        return res.status(500).json(err);
    })
    })

    router.get('/getUser',(req, res) => {
        connection.query('SELECT * FROM client ',(err, result)=>{
            if(err){console.log(err);}
            if(result.length>0)
            {
                  res.send({
                    message :'all data',
                    data:result
                  });
            }
            else
            res.send({
                message :'data not found',
              });
        });
    })

    router.get('/getUser/:id_client',(req, res) => {
        const id_client= req.params.id_client;
        connection.query('SELECT * FROM client where id_client=?',id_client,(err, result)=>{
            if(err){console.log(err);}
            if(result.length>0)
            {
                  res.send({
                    message :'get single data',
                    data:result
                  });
            }
            else
            res.send({
                message :'data not found',
              });
        });
    })


module.exports = router;