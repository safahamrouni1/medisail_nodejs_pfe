const express = require('express');
const connection= require ('../connection');
const router = express.Router();
const request = require('request');
var auth = require('../services/authentification');
var checkRole = require('../services/checkRole');
const apiKey = '4709649506eca5dfe4ba816cd7e2a343';


router.post('/weather',(req,res)=>{
  let city = req.body.city;

  console.log(city);
  const url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}&units=metric`;

  request(url, (error, response, body) => {
    if (error) {
      console.log(error);
      res.status(500).json({ message: 'Error retrieving weather data' });
    } else {
      const weatherData = JSON.parse(body);
      res.json(weatherData);
    }
  }
       
  )})



router.get('/getweather/', (req, res) => {
    const city = req.params.city;
    console.log(city ,'cityreq')
  
    const url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}&units=metric`;
  
    request(url, (error, response, body) => {
      if (error) {
        console.log(error);
        res.status(500).json({ message: 'Error retrieving weather data' });
      } else {
        const weatherData = JSON.parse(body);
        res.json(weatherData);
      }
    });
  });

module.exports = router;