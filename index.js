const express = require('express');
var cors =require ('cors');
const connection = require('./connection');
const userRoute = require("./routes/user");
const notificationRoute = require("./routes/notification");
const boatRoute = require("./routes/boat");
const tokenRoute = require("./routes/token");
const alertRoute = require("./routes/alert");
const weatherRoute = require("./routes/weather");
const historyRoute = require("./routes/history");
const app = express();

app.use(cors());
app.use(express.urlencoded({extended:true}));
app.use(express.json());
app.use('/user',userRoute)
app.use('/notification',notificationRoute)
app.use('/boat',boatRoute)
app.use('/token',tokenRoute)
app.use('/alert',alertRoute)
app.use('/weather',weatherRoute)
app.use('/history',historyRoute)
module.exports = app;