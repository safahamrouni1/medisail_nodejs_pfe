const mysql = require('mysql');
require('dotenv').config();

var connection =mysql.createConnection({
    port: process.env.DATABASE_PORT,
    host: process.env.DATABASE_HOST,
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: 'medisail_pfe',
});

connection.connect((err)=> {
    if(!err)
    {
        console.log("MySQL connected");
    }
    else
    console.log(err);
});

module.exports = connection;